(in-package :cl-torrent)

(defun make-unique-peer-id ()
  (let ((prng (ironclad:make-prng :fortuna :seed :urandom)))
    (flexi-streams:octets-to-string (ironclad:random-data 20 prng) :external-format :ISO-8859-1)))

(defclass system-info ()
  ((peer-id :reader peer-id
            :documentation
            "Random peer id used to identify self for session. Set on creation.")
   (ip :accessor ip :initarg :ip)
   (port :accessor port :initarg :port))
  (:documentation
   "Session-wide system information about this client, such
as their peer-id, ip, etc."))

(defmethod initialize-instance :after ((info system-info) &key)
  (setf (slot-value info 'peer-id) (make-unique-peer-id)))
