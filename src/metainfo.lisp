(in-package :cl-torrent)

(defun read-metainfo-from-file (filepath)
  "read the metainfo file in from the given filepath and
bencode it to it's dictionary form"
  (read-metainfo-to-dict filepath))

(defun read-metainfo-to-dict (filepath)
  "read the metainfo file in from the given filepath and
bencode it to it's dictionary form"
  (with-open-file (stream filepath :element-type '(unsigned-byte 8))
    (bencode:decode stream)))

(defun read-metainfo-to-list (filepath)
  "read the metainfo file in from the given filepath and
bencode it to it's dictionary form"
  (let ((bencode::*use-list-for-dictionary* t))
    (with-open-file (stream filepath :element-type '(unsigned-byte 8))
      (bencode:decode stream))))

;; top level metainfo object
(defclass metainfo-file ()
  ((announce :initarg :announce)
   (announce-list :initarg :announce-list)
   (comment :initarg :comment)
   (created-by :initarg :created-by)
   (creation-date :initarg :creation-date)
   (info :initarg :info)))

;; the info entry for a torrent that is
;; only a single file
(defclass info-single-file ()
  ((length :initarg :length)
   (md5sum :initarg :md5sum)
   (name :initarg :name)
   (piece-length :initarg :piece-length)
   (pieces :initarg :pieces)))


;; the info entry for a torrent that contains
;; multiple files
(defclass info-multi-file ()
  ((files :initarg :files)
   (name :initarg :name)
   (piece-length :initarg :piece-length)
   (pieces :initarg :pieces)))

;; an individual file in an info-multi-file#files
(defclass multi-file ()
  ((length :initarg :length)
   (md5sum :initarg :md5sum)
   (path :initarg :path)))

;;;;

(defun create-multi-files (files)
  "create the object representation of a single file in a
multi-file torrent info"
  (loop for f in files
        collect
        (make-instance 'multi-file
                       :length (gethash "length" f)
                       :md5sum (gethash "md5sum" f)
                       :path (gethash "path" f))))

(defun create-info (info-hash)
  "create the object representation of the info dictionary value"
  (defun getinfo (k)
    (gethash k info-hash))
  (let ((is-single-file-info? (not (nth-value 1 (getinfo "files")))))
    (if is-single-file-info?
        (make-instance 'info-single-file
                       :length (getinfo "length")
                       :md5sum (getinfo "md5sum")
                       :name (getinfo "name")
                       :piece-length (getinfo "piece-length")
                       :pieces (getinfo "pieces"))
        (make-instance 'info-multi-file
                       :files (create-multi-files (getinfo "files"))
                       :name (getinfo "name")
                       :piece-length (getinfo "piece length")
                       :pieces (getinfo "pieces")))))

(defun create-metainfo-file (filepath)
  "create a single object that represents the dictionary
that is the metainfo file."
  (let* ((decoded-metainfo (read-metainfo-from-file filepath))
         (info (gethash "info" decoded-metainfo)))
    (make-instance 'metainfo-file
                   :announce (gethash "announce" decoded-metainfo)
                   :announce-list (gethash "announce-list" decoded-metainfo)
                   :comment (gethash "comment" decoded-metainfo)
                   :created-by (gethash "created by" decoded-metainfo)
                   :creation-date (gethash "creation date" decoded-metainfo)
                   :info (create-info info))))
