(in-package :cl-torrent)

(defun map-to-string (&rest args)
  "convert args to list of strings"
  (flet ((to-string (x) (if (simple-string-p x) x (write-to-string x))))
    (mapcar #'to-string args)))

(defun calc-info-sha1 (metainfo)
  "Calculate the sha1 from our .torrent file"
  (let* ((info (bencode:encode (gethash "info" metainfo) nil))
         (raw-info-hash (ironclad:digest-sequence :sha1 info))
         (info-sha1 (flexi-streams:octets-to-string raw-info-hash :external-format :ISO-8859-1)))
    info-sha1))

(defun tracker-announce (torrent-info system-info
                         &key (event "started") (compact 1))
  "Announce to the tracker for this torrent.
Tracker will return back list of peers and other
information."
  (let* ((metainfo (slot-value torrent-info 'metainfo))
         (announce-url  (gethash "announce" metainfo))
         (info-sha1 (calc-info-sha1 metainfo)))
    (with-slots (peer-id ip port) system-info
      (with-slots (numwant uploaded downloaded left) torrent-info
        (drakma:http-request announce-url :parameters
                             (pairlis
                              '("compact" "info_hash" "peer_id" "port"
                                "uploaded" "downloaded" "left" "event")
                              (map-to-string
                               compact info-sha1 peer-id port
                               uploaded downloaded left event)))))))

(defun create-ip-port-tuples (peer-byte-list peer-count)
  "Used for uncompacting a raw bytes of peers."
  ;; each sextuplet is (ip(4), port(2))
  (loop for i from 0 to (- peer-count 1)
     collect
       (let* ((start-ip (* i 6))
              (end-ip (+ start-ip 4))
              (start-port end-ip)
              (end-port (+ start-port 2))
              (ip  (subseq peer-byte-list start-ip end-ip))
              (port (subseq peer-byte-list start-port end-port)))
         (list (format nil "~{~a~^.~}" ip)
               (+ (ash (car port) 8)
                  (cadr port))))))

(defun uncompact-peers (peers-str)
  "Take a raw string of compacted peers from a tracker
announce and return ipv4:port tuples."
  (let* ((peers-vec (flexi-streams:string-to-octets peers-str :external-format :ISO-8859-1))
         (peers-list (map 'list #'identity peers-vec))
         (number-of-peers (/ (length peers-vec) 6)))
    (create-ip-port-tuples peers-list number-of-peers)))

(defun decode-tracker-announce (resp)
  (handler-case
      (bencode:decode resp :external-format :ISO-8859-1)
    (bencode::invalid-value-type (e) nil)
    (end-of-file (e) nil)))

(defun tracker-init (torrent-info system-info
                     &key (compact 1))
  "Announce to tracker and decode response. Decoded peers key
is 'peers-decoded'."
  (let* ((resp (tracker-announce torrent-info system-info :compact compact))
         (decoded-resp (decode-tracker-announce resp))
         (uncompacted-peers (uncompact-peers (gethash "peers" decoded-resp))))
    (setf (gethash "peers-decoded" decoded-resp) uncompacted-peers)
    decoded-resp))
