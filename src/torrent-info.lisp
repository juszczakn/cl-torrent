(in-package :cl-torrent)

(defclass torrent-info ()
  ((metainfo
    :initarg :metainfo
    :reader metainfo
    :initform (error
               "Must be tied to a .torrent file"))
   (numwant
     :initarg :numwant
     :accessor numwant
     :initform 0)
   (uploaded
    :initarg :uploaded
    :accessor uploaded
    :initform 0)
   (downloaded
    :initarg :downloaded
    :reader downloaded
    :initform 0)
   (left
    :initarg :left
    :accessor left
    :initform (error
               "Must specify bytes left in download")))
  (:documentation
   "Misc torrent information such as number of peers
wanted, amounts of bytes uploaded, downloaded, left, etc."))
