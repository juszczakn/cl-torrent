(defsystem "cl-torrent"
  :description "cl-torrent: library implementing bittorrent protocol"
  :version "0.0.1"
  :author "Nick Juszczak <juszczakn@gmail.com>"
  :licence "BSD"

  :depends-on ("bencode" "drakma" "ironclad" "flexi-streams")
  :serial t
  :components ((:module
                 "src"
                 :components
                 ((:file "packages")
                  (:file "util")
                  (:file "metainfo")
                  (:file "system-info")
                  (:file "torrent-info")
                  (:file "tracker")))))
